# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## Added
- Added Basic Layout, MUI setup, Routing, Navigation
- Added support for Docker
- Added helm-chart
- Added basePath and enabled trailingSlash
- Added catalog page, models details page, enable form.
- Added playground page.
- Added static pages for dashboard feature.
- Added playgound parameters functionalites and alignment 
- Added breadcrumbs
- Added Charts and Table (Static UI) for dashboard page
- Added compare models functionality added in playground
- Added models search and select filters
- Added playground alignment changes
- Added model details page with tabs with markdown content rendering, tabs and enable form.
- Added theme configurations and cleaned inline stylings
- Added theme background and primary colour overrides
- Added vercel sdk with marked, models list multi select and multi model stream api integration.
- Show error message when Model responds with an error
- Token count and cost to ModelResponseCard
- Added parameters to api
- Added selected models list and prompt to the local storage
- OAuth2 authentication using envoy proxy
- LLM Access Log page and API
- Cost, Tokens and Request charts on Dashboard
- Added Coming soon routes
- Added 'Applications' page

## Changed
- Downgrade next to 13.5.2 as Images are not working behind proxy,
    see [56038](https://github.com/vercel/next.js/issues/56038),
- Changed playground layout.
- Disable Submit button when no Models are selected
- Improve ModelResponseCard
- Changed min, max and default values of parameters
- Changed functionality of 'Clear' button in the playground to clear llm response, token & cost in the cards
- Changed model catalog details page 

## Removed
- Removed tabs in playground page
- Removed parameters (repetition, top_k)

## Fixed
- Fixed yarn build issue 'children' between tags not as props
- Fixed styling in pages
- Automatic trigger to API when a new Model is selected
- Fixed max, min and default 'max_tokens' values on selected list of models
