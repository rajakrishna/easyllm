// import { Roboto } from 'next/font/google';
import { createTheme } from '@mui/material/styles';

// const roboto = Roboto({
//   weight: ['300', '400', '500', '700'],
//   subsets: ['latin'],
//   display: 'swap',
// });

const theme = createTheme({
  palette: {
    mode: 'light',
    primary: {
      // main: '#007BFF',
     main:'#27AAE1',
     contrastText: '#fff',
     },
    background: {
      default: `rgb(250, 250, 251)`, // Replace this with your desired background color
    },
  },
  // typography: {
  //   fontFamily: roboto.style.fontFamily,
  // },
  components: {
    MuiAlert: {
      styleOverrides: {
        root: ({ ownerState }) => ({
          ...(ownerState.severity === 'info' && {
            backgroundColor: '#60a5fa',
          }),
        }),
      },
    },
  },
});

export default theme;
