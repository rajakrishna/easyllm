/** @type {import('next').NextConfig} */
const nextConfig = {
    output: 'standalone',
    basePath: '/easyllm',
    trailingSlash: true,
}

module.exports = nextConfig
