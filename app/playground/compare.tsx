// components/Compare.tsx
'use client'

import React, {useEffect, useState} from 'react'
import {Box, Grid, TextField, Button, Card} from '@mui/material'
import ModelResponseCard from './modelResponseCard'
import Parameters from './parameters'
import {ModelConfig} from './playgroundData'

export default function Compare() {
  const [selectedModels, setSelectedModels] = useState<ModelConfig[]>([])
  const [prompt, setPrompt] = useState<string>('')
  const [submitPrompt, setSubmitPrompt] = useState<string>('');
  const [clearResponse, setClearResponse] = useState<string>('')
  const [sliderValues, setSliderValues] = useState<Record<string, number>>({
    "max_tokens":256,
    "temperature": 1,
    "top_p": 1,
    "frequency_penalty": 0,
    "presence_penalty": 0
  });
  const [stopSequenceValue, setStopSequenceValue] = useState<string>('');

  const handleStopSequenceValue = (value: string) => {
    setStopSequenceValue(value);
  }

  const handleSliderValues = (paramId: string, value: string) => {
    const parsedValue = value === '' ? '0' : value
    setSliderValues((prevValues) => ({
      ...prevValues,
      [paramId]: parseFloat(parsedValue),
    }))
  }

  useEffect(() => {
    const modeslFromLocalStorage = localStorage.getItem('selectedModels');
    if (modeslFromLocalStorage) {
      const parsedModelsList = JSON.parse(modeslFromLocalStorage);
      const maxTokensList = parsedModelsList.map((model: { max_tokens: any }) => model.max_tokens);
      const maxTokenValue = maxTokensList.length ? Math.max(...maxTokensList) : 256;
      setSelectedModels(JSON.parse(modeslFromLocalStorage));
      const currentMaxTokenValue = sliderValues.max_tokens;
      if (currentMaxTokenValue > maxTokenValue) {
        setSliderValues((prevValues) => ({
          ...prevValues,
          max_tokens: maxTokenValue,
        }));
      }
    }

    const promptFromLocalStorage = localStorage.getItem('promptString');
    if (promptFromLocalStorage) {
      setPrompt(promptFromLocalStorage);
    }
  }, [])


  const handleModelsSelected = (event: React.ChangeEvent<{}>, newValue: ModelConfig[]) => {
    const maxTokensList = newValue.map(model => model.max_tokens);
    const maxTokenValue = maxTokensList.length ? Math.max(...maxTokensList) : 256;
    setSubmitPrompt("");
    setSelectedModels(newValue);
    localStorage.setItem('selectedModels', JSON.stringify(newValue));
    const currentMaxTokenValue = sliderValues.max_tokens;
      if (currentMaxTokenValue > maxTokenValue) {
        setSliderValues((prevValues) => ({
          ...prevValues,
          max_tokens: maxTokenValue,
        }));
      }
  }

  const responsesPerRow = selectedModels.length === 1 ? 12 : 6

  const handlePromptChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPrompt(event.target.value);
    localStorage.setItem('promptString', event.target.value);
  }

  const clearPrompt = () => {
    setPrompt('');
    localStorage.removeItem('promptString');
  }

  const handleSubmit = () => {
    console.log(`Prompt: ${prompt}`)
    setSubmitPrompt('submit')
  }

  const setSubmitPromptToEmpty = () => {
    setSubmitPrompt('')
  }

  const handleClearResponse = () => {
    clearPrompt();
    setClearResponse('clear')
  }

  const setClearResponseToEmpty = () => {
    setClearResponse('')
  }

  return (
    <Box>
      <Grid container spacing={2}>

        <Grid item xs={6} md={9}>
          <Card>
            <Box padding={1}>
              <TextField
                fullWidth
                id='outlined-basic'
                label='Promt here...'
                variant='outlined'
                multiline
                rows={10}
                value={prompt}
                onChange={handlePromptChange}
              />

              <Box pt={2} pb={1}>
                <Button
                  sx={{
                    borderRadius:50,
                    pl:3,
                    pr:3,
                    mr: 1
                  }}
                  variant='contained'
                  color='primary'
                  disableElevation={true}
                  disabled={!selectedModels.length || !prompt.length}
                  size='small'
                  onClick={handleSubmit}
                >
                  Submit
                </Button>
                <Button
                  sx={{
                    borderRadius:50,
                    pl:3,
                    pr:3
                  }}
                  size='small'
                  variant='outlined'
                  onClick={handleClearResponse}
                >
                  Clear
                </Button>
              </Box>
            </Box>
          </Card>

          <Grid container spacing={2} marginTop={1}>
            {selectedModels.map((model) => {
              return (
                <Grid item xs={responsesPerRow} key={model.id}>
                  <ModelResponseCard
                    provider={model.provider}
                    model={model.value}
                    prompt={prompt}
                    submitPrompt={submitPrompt}
                    sliderValues={sliderValues}
                    stopSequenceValue={stopSequenceValue}
                    setSubmitPromptToEmpty={setSubmitPromptToEmpty}
                    clearResponse={clearResponse}
                    setClearResponseToEmpty={setClearResponseToEmpty}
                  />
                </Grid>
              )
            })}
          </Grid>
        </Grid>

        <Grid item xs={6} md={3}>
          <Parameters 
            sliderValues={sliderValues} 
            handleSliderValues={handleSliderValues} 
            handleModelsSelected={handleModelsSelected} 
            selectedModels={selectedModels} 
            stopSequenceValue={stopSequenceValue}
            handleStopSequenceValue={handleStopSequenceValue}
          />
        </Grid>
      </Grid>
    </Box>
  )
}
