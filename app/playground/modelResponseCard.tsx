import {
  Box,
  Card,
  CardContent,
  List,
  ListItem,
  Typography,
  Alert,
  CardActions,
  Chip,
  CardHeader,
  Avatar,
} from '@mui/material'
import {marked} from 'marked'
import React, {useEffect, useState} from 'react'
import {Message, useChat} from 'ai/react'
import {modelsData} from './playgroundData'

type SliderTypeRecord = Record<string, number>
interface ModelResponseProps {
  provider: string
  model: string
  prompt: string
  submitPrompt: string
  sliderValues: SliderTypeRecord
  stopSequenceValue: string
  setSubmitPromptToEmpty: () => void
  clearResponse : string
  setClearResponseToEmpty: () => void
}

interface ModelResponseCost {
  prompt_tokens: number
  completion_tokens: number
  total_tokens: number
  prompt_cost: number
  completion_cost: number
  total_cost: number
}

const ModelResponseCard: React.FC<ModelResponseProps> = ({
  provider,
  model,
  prompt,
  submitPrompt,
  sliderValues,
  stopSequenceValue,
  setSubmitPromptToEmpty,
  clearResponse,
  setClearResponseToEmpty
}) => {
  const [llmResponseError, setLlmResponseError] = useState<any>({})
  const [previousResponse, setPreviousResponse] = useState<any>({})
  const [cost, setCost] = useState<ModelResponseCost>({
    prompt_tokens: 0,
    completion_tokens: 0,
    total_tokens: 0,
    prompt_cost: 0,
    completion_cost: 0,
    total_cost: 0,
  })
  const {messages, append} = useChat({
    api: '/easyllm/api/chat/',
    onError: () => {
      const errorObject = {
        "id": "error",
        "createdAt": "",
        "content": "Something went wrong. Please try again later.",
        "role": ""
    }
      setLlmResponseError(errorObject)
      localStorage.setItem(`${model}`, JSON.stringify(errorObject))
    },
    onFinish(message) {
      getCost(prompt, message)
      setSubmitPromptToEmpty()
      localStorage.setItem(`${model}`, JSON.stringify(message))
    },
  })

  const getCost = (prompt: string, message: Message) => {
    fetch('/easyllm/api/cost/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        model: `${provider}/${model}`,
        messages: [{role: 'user', content: prompt}],
        completion: message.content,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        setCost(data);
        const prevModel = localStorage.getItem(`${model}`);
        if (prevModel) {
          const prevModelWithCost = {...JSON.parse(prevModel), total_cost: data.total_cost, total_tokens: data.total_tokens};
          setPreviousResponse(prevModelWithCost)
          localStorage.setItem(`${model}`, JSON.stringify(prevModelWithCost))
        }
      })
  }

  useEffect(() => {
    console.log('Child Prompt: ', prompt)
    if (submitPrompt.length) {
      setLlmResponseError({})
      handleSubmit(prompt)
    } else {
      console.log('Prompt Skipping useChat')
    }
    const previousResponse = localStorage.getItem(`${model}`)
    if (previousResponse) {
      setPreviousResponse(JSON.parse(previousResponse))
    }
    if (clearResponse.length) {
      setPreviousResponse({});
      localStorage.removeItem(`${model}`);
      setClearResponseToEmpty();
    }
  }, [submitPrompt, clearResponse])

  const handleSubmit = (prompt: string) => {
    console.log(`Prompt: ${prompt}`)
    localStorage.removeItem(`${model}`);
    setPreviousResponse({});
    append(
      {
        role: 'user',
        content: prompt,
      },
      {
        options: {
          body: {
            provider,
            model,
            max_tokens: sliderValues.max_tokens,
            temperature: sliderValues.temperature,
            top_p: sliderValues.top_p,
            top_k: sliderValues.top_k,
            frequency_penalty: sliderValues.frequency_penalty,
            presence_penalty: sliderValues.presence_penalty,
            stop: `${stopSequenceValue}`,
          },
        },
      }
    )
  }

  const codeBlock = (code: string) => {
    const markedCode = marked.parse(code, {breaks: true})
    return <Box dangerouslySetInnerHTML={{__html: markedCode}} />
  }

  const renderMarkedResponse = (msg: any) => {
    const tokens = marked.lexer(msg)
    return (
      <div>
        {tokens.map((token: any) => {
          let tokenItem: any
          switch (token.type) {
            case 'paragraph':
              tokenItem = (
                <Typography variant='body2' paragraph={true} gutterBottom>
                  {token.text}
                </Typography>
              )
              break
            case 'code':
              tokenItem = <Box mb={2}>{codeBlock(token.raw)}</Box>
              break
            case 'space':
              tokenItem = <Typography variant='body2' paragraph={true} gutterBottom></Typography>
              break
            case 'list':
              tokenItem = (
                <List style={{listStyleType: 'none'}}>
                  {token.items.map((listItem: any) => (
                    <ListItem key={listItem.raw}>{listItem.raw}</ListItem>
                  ))}
                </List>
              )
              break

            default:
              break
          }
          return tokenItem
        })}
      </div>
    )
  }

  return (
    <Card>
      <CardHeader
        title={model}
        subheader={provider}
        avatar={<Avatar src={modelsData.find((m) => m.value === model)?.logo} />}
      />
      <CardContent>
        {llmResponseError.content ? (
          <Alert severity='error'>{llmResponseError.content}</Alert>
        ) : (
          <Box>
            {previousResponse.content ? 
            <Typography variant='body2'>
              {previousResponse.id === 'error'
              ?
              <Alert severity='error'>{previousResponse.content}</Alert>
              :
              renderMarkedResponse(previousResponse.content)}
            </Typography>
            :
            <Typography variant='body2'>
              {messages?.slice(-1).map((m) => {
                return m.role === 'assistant' && renderMarkedResponse(m.content)
              })}
            </Typography>
            }
          </Box>
        )}
      </CardContent>
      <CardActions>
        <Chip label={`${previousResponse.total_tokens ? previousResponse.total_tokens : cost.total_tokens} tokens`} size='small' />
        <Chip label={`$ ${previousResponse.total_tokens ? previousResponse.total_cost : cost.total_cost}`} size='small' />
      </CardActions>
    </Card>
  )
}

export default ModelResponseCard
