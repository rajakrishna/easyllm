'use client'
import React from 'react'
import {
  Box,
  Grid,
  TextField,
  Typography,
  Card,
  Autocomplete,
  Slider,
  ListItem,
  ListItemText,
} from '@mui/material'
import MuiInput from '@mui/material/Input'
import {styled} from '@mui/material/styles'
import {ModelConfig, modelsData, parametersData} from './playgroundData'

const Input = styled(MuiInput)`
  width: 42px;
`
type SliderTypeRecord = Record<string, number>;
interface ChildProps {
  handleModelsSelected: (event: React.ChangeEvent<{}>, data: ModelConfig[]) => void
  selectedModels: Array<ModelConfig>
  sliderValues: SliderTypeRecord
  handleSliderValues: (paramId: string, data: any) => void
  stopSequenceValue: string
  handleStopSequenceValue: (value: string) => void
}

export default function Parameters(props: ChildProps) {
  const [sliderValues, setSliderValues] = React.useState<Record<string, number>>({})

  const getMaxValue = () => {
    let maxTokenValue = 4096;
    const modelsFromProps = props.selectedModels;
    if (modelsFromProps) {
      const maxTokensList = modelsFromProps.map((model: { max_tokens: any }) => model.max_tokens);
      maxTokenValue = maxTokensList.length ? Math.max(...maxTokensList) : 4096;
    }
    return maxTokenValue;
  }	

  const handleBlur = (paramId: string) => {
    const value = sliderValues[paramId]
    const min = parametersData.find((param) => param.id === paramId)?.min || 0
    const max = parametersData.find((param) => param.id === paramId)?.max || 100

    if (value < min) {
      setSliderValues((prevValues) => ({
        ...prevValues,
        [paramId]: min,
      }))
      props.handleSliderValues(paramId, min)
    } else if (value > max) {
      setSliderValues((prevValues) => ({
        ...prevValues,
        [paramId]: max,
      }))
      props.handleSliderValues(paramId, max)
    }
  }

  const handleInputChange = (paramId: string, newValue: string) => {
    const parsedValue = newValue === '' ? '0' : newValue
    setSliderValues((prevValues) => ({
      ...prevValues,
      [paramId]: parseFloat(parsedValue),
    }))
    props.handleSliderValues(paramId, newValue)
  }

  const handleSliderChange = (paramId: string, newValue: number) => {
    setSliderValues((prevValues) => ({
      ...prevValues,
      [paramId]: newValue,
    }))
    props.handleSliderValues(paramId, newValue)
  }

  return (
    <Card>
      <Box p={2} pb={0}>
        <Box pb={2}>
          <Typography>Parameters</Typography>
        </Box>
        <Autocomplete
          multiple
          id='my-multi-select-autocomplete'
          options={modelsData}
          disableCloseOnSelect
          getOptionLabel={(option) => option.label}
          onChange={props.handleModelsSelected}
          value={props.selectedModels}
          renderOption={(props, option, {selected}) => (
            <ListItem {...props}>
              <ListItemText primary={option.label} />
            </ListItem>
          )}
          renderInput={(params) => (
            <TextField {...params} variant='outlined' label='Select Model' placeholder='' />
          )}
        />
      </Box>

      <Box padding={2}>
        {parametersData.map((param) => {
          return (
            <Box px={2} key={param.id}>
              <Grid container>
                <Grid xs={9}>
                  <Typography id={`input-slider-${param.id}`} gutterBottom>
                    {param.name}
                  </Typography>
                </Grid>

                <Grid xs={3}>
                  <Input
                    sx={{width: `4rem`}}
                    value={props.sliderValues[param.id] || param.default}
                    defaultValue={param.default}
                    size='small'
                    onChange={(event) => handleInputChange(param.id, event.target.value)}
                    onBlur={() => handleBlur(param.id)}
                    fullWidth={false}
                    disableUnderline
                    inputProps={{
                      step: {value: param.step},
                      min: {value: param.min},
                      max: {value: param.id === 'max_tokens' ? getMaxValue() : param.max},
                      type: 'number',
                      'aria-labelledby': `input-slider-${param.id}`,
                    }}
                  />
                </Grid>
              </Grid>
              <Slider
                value={props.sliderValues[param.id] || param.default}
                defaultValue={param.default}
                onChange={(_, newValue) => handleSliderChange(param.id, newValue as number)}
                aria-labelledby={`input-slider-${param.id}`}
                marks={false}
                min={param.min}
                max={param.id === 'max_tokens' ? getMaxValue() : param.max}
                step={param.step}
              />
            </Box>
          )
        })}

        <Box pt={2}>
          <TextField
            fullWidth
            id='outlined-basic'
            label='Stop sequences'
            placeholder='Enter sequence and press Tab'
            variant='outlined'
            value={props.stopSequenceValue}
            onChange={(e) => props.handleStopSequenceValue(e.target.value)}
          />
        </Box>
      </Box>
    </Card>
  )
}
