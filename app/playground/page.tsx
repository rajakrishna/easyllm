import * as React from 'react'
import {Box, Typography} from '@mui/material'
import Compare from './compare'

export default function Page() {
  return (
    <Box paddingRight={10} paddingLeft={5}>
      <Typography gutterBottom paddingBottom={2} marginBottom={0} variant='h6' component='div'>
        Playground
      </Typography>
      <Compare />
    </Box>
  )
}
