import './globals.css'
import type { Metadata } from 'next'
import { Inter } from 'next/font/google'
import AppLayout from './appLayout'
import ThemeRegistry from '@/components/ThemeRegistry'

const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: 'EasyLLM',
  description: 'Interoperate between LLMs with fast finetuning',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
        <body className={inter.className}>
      <ThemeRegistry>
        <AppLayout>
          {children}
        </AppLayout>
      </ThemeRegistry>
        </body>
    </html>
  )
}
