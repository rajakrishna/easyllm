import visualisation from './catalog/genai/visualisation.png';
import pricing from './catalog/genai/pricing.png';


export const DashboardCatalog = [
    {
        "id": 9,
        "name": "Visualizations",
        "sourceType": "VISUALIZATIONS",
        "finetuned": false,
        "description": "Help you understand the patterns, trends, and insights within the data more effectively",
        "Type": "text-generation",
        "category": "visualizations",
        "status": "Available",
        "primary_dashboard": "https://stage.predera.com/",
        "model_ref_id": "f662401899a94b5f98253579b163a99d",
        "additional_report_ids": "\"2239\", \"1193\"",
        "created_at": "2022-07-05T10:23:51.229208",
        "created_by": "srikanth.thirumala@predera.com",
        "updated_at": "2022-07-05T10:23:51.229208",
        "updated_by": "srikanth.thirumala@predera.com",
        "app_image_url": visualisation,
        "app_id": "Llama 2",
        "versions": [
            { 
                "value": "Llama-2-7b", 
                "label": "Llama-2-7b",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 1
                }
            },
            { 
                "value": "Llama-2-13b", 
                "label": "Llama-2-13b",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 2
                }
             },
            { 
                "value": "Llama-2-70b", 
                "label": "Llama-2-70b",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 8
                }
            }
        ]
    },
    {
        "id": 10,
        "name": "Pricing",
        "sourceType": "PRICING",
        "finetuned": false,
        "description": "Llama 2 fine-tuned and optimized for dialogue use cases.",
        "Type": "text-generation",
        "category": "pricing",
        "status": "Available",
        "primary_dashboard": "https://stage.sandbox.predera.com/id",
        "model_ref_id": "94da4f6c7c9649e5836c310766449bd1",
        "additional_report_ids": "\"1123\", \"3494\"",
        "created_at": "2022-07-05T10:20:22.515729",
        "created_by": "vijaykumar.guntreddy@predera.com",
        "updated_at": "2022-07-05T10:20:22.515729",
        "updated_by": "srikanth.thirumala@predera.com",
        "app_image_url": pricing,
        "app_id": "Llama-2-Chat",
        "versions": [
            { 
                "value": "Llama-2-7b-chat", 
                "label": "Llama-2-7b-chat",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 1
                }
             },
            { 
                "value": "Llama-2-13b-chat", 
                "label": "Llama-2-13b-chat",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 2
                }
            },
            { 
                "value": "Llama-2-70b-chat", 
                "label": "Llama-2-70b-chat",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 8
                }
            },
        ]
    },
    // {
    //     "id": 1,
    //     "name": "Prepare Data",
    //     "sourceType": "PREPARE_DATA",
    //     "finetuned": false,
    //     "description": "Most capable GPT-3.5 model and optimized for chat.",
    //     "Type": "text-generation",
    //     "category": "prepareData",
    //     "status": "Available",
    //     "primary_dashboard": "https://stage.sandbox.com/id",
    //     "model_ref_id": "94da4f6c7c9649e5836c310766449bd1",
    //     "additional_report_ids": "\"1125\", \"5294\"",
    //     "created_at": "2022-07-05T10:19:59.670676",
    //     "created_by": "srikanth.thirumala@predera.com",
    //     "updated_at": "2022-07-05T10:19:59.670676",
    //     "updated_by": "vijaykumar.guntreddy@predera.com",
    //     "app_image_url": data,
    //     "app_id": "PrepareData",
    //     "versions": [
    //         {
    //             "value": "gpt-3.5-turbo-0301", 
    //             "label": "gpt-3.5-turbo-0301",
    //             "metrics": {
    //                 "cpu": 0.5,
    //                 "memory": 0.5,
    //                 "gpu": 0
    //             }
    //         },
    //         {
    //             "value": "gpt-3.5-turbo-0613", 
    //             "label": "gpt-3.5-turbo-0613","metrics": {
    //                 "cpu": 0.5,
    //                 "memory": 0.5,
    //                 "gpu": 0
    //             }
    //         }
    //     ]
    // },
    // {
    //     "id": 20,
    //     "name": "Model Development",
    //     "sourceType": "MODEL_DEVELOPMENT",
    //     "finetuned": false,
    //     "description": "Deploy any model from huggingface.",
    //     "Type": "text-generation",
    //     "category": "huggingface",
    //     "status": "Available",
    //     "primary_dashboard": "https://stage.sandbox.predera.com/id",
    //     "model_ref_id": "94da4f6c7c9649e5836c310766449bd1",
    //     "additional_report_ids": "\"1123\", \"3494\"",
    //     "created_at": "2022-07-05T10:20:22.515729",
    //     "created_by": "vijaykumar.guntreddy@predera.com",
    //     "updated_at": "2022-07-05T10:20:22.515729",
    //     "updated_by": "srikanth.thirumala@predera.com",
    //     "app_image_url": modelDevelopment,
    //     "app_id": "Code-Assist",
    //     "versions": [
    //         { 
    //             "value": "Code-Assist-7b", 
    //             "label": "Code-Assist-7b",
    //             "metrics": {
    //                 "cpu": 0.5,
    //                 "memory": 0.5,
    //                 "gpu": 1
    //             }
    //          },
    //         { 
    //             "value": "Code-Assist-13b", 
    //             "label": "Code-Assist-13b",
    //             "metrics": {
    //                 "cpu": 0.5,
    //                 "memory": 0.5,
    //                 "gpu": 2
    //             }
    //         },
    //         { 
    //             "value": "Code-Assist-70b", 
    //             "label": "Code-Assist-70b",
    //             "metrics": {
    //                 "cpu": 0.5,
    //                 "memory": 0.5,
    //                 "gpu": 8
    //             }
    //         },
    //     ]
    // }
]