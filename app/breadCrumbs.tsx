"use client"

import * as React from 'react';
import { usePathname } from 'next/navigation';
import { Box, Breadcrumbs, Typography } from '@mui/material';
import Link from 'next/link';

function handleClick(event: React.MouseEvent<HTMLDivElement, MouseEvent>) {
  event.preventDefault();
  console.info('You clicked a breadcrumb.');
}

export default function Breadcrumb() {
  const pathName = usePathname();
  const pathnames = pathName.split('/').filter((x) => x);
  const firstItem = (path: string) => {
    let result
    switch (path) {
      case 'catalog':
        return "/"
      case 'playground':
        return "/"
      case 'accesslogs':
        return "/"
      case 'applications':
        return "/"
    
      default:
        return path;
    }
  }
  return (
    <Box paddingLeft={3}>
      <Breadcrumbs separator={"/"}>
      {firstItem(pathnames[0]) !== '/' && <Link href="/">
        {pathnames.length 
        ? 
        <Typography color="primary">Dashboard</Typography> 
        : 
        <Typography>Dashboard</Typography>
        }
      </Link>}
      {pathnames.map((path, index) => {
        const isLast = index === pathnames.length - 1;
        const routeTo = `/${path}`;
        return isLast ? (
          <Typography key={path}>{path[0].toUpperCase() + path.slice(1)}</Typography>
        ) : (
          <Link key={path} href={routeTo}>
            <Typography color="primary" key={path}>{path[0].toUpperCase() + path.slice(1)}</Typography>
          </Link>
        );
      })}
      </Breadcrumbs>
    </Box>
  );
}