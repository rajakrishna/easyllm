import { Box } from "@mui/material"
import Visualizations from "./visualizations"

export default function Page() {
    return (
        <Box paddingLeft={4}>
            <Visualizations />
        </Box>
    )
  }