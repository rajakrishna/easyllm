import data from '../catalog/genai/data.png';
import modelDevelopment from '../catalog/genai/modelDevelopment.png';
import visualisation from '../catalog/genai/visualisation.png';
import pricing from '../catalog/genai/pricing.png';


export const VisualizationsCatalog = [
    {
        "id": 1,
        "name": "Bar Chart",
        "sourceType": "BAR_CHART",
        "finetuned": false,
        "description": "Help you understand the patterns, trends, and insights within the data more effectively",
        "Type": "text-generation",
        "category": "barChart",
        "status": "Available",
        "primary_dashboard": "https://stage.predera.com/",
        "model_ref_id": "f662401899a94b5f98253579b163a99d",
        "additional_report_ids": "\"2239\", \"1193\"",
        "created_at": "2022-07-05T10:23:51.229208",
        "created_by": "srikanth.thirumala@predera.com",
        "updated_at": "2022-07-05T10:23:51.229208",
        "updated_by": "srikanth.thirumala@predera.com",
        "app_image_url": visualisation,
        "app_id": "Llama 2",
        "versions": [
            { 
                "value": "Llama-2-7b", 
                "label": "Llama-2-7b",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 1
                }
            },
            { 
                "value": "Llama-2-13b", 
                "label": "Llama-2-13b",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 2
                }
             },
            { 
                "value": "Llama-2-70b", 
                "label": "Llama-2-70b",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 8
                }
            }
        ]
    },
    {
        "id": 2,
        "name": "Line Chart",
        "sourceType": "LINE_CHART",
        "finetuned": false,
        "description": "Deploy any model from huggingface.",
        "Type": "text-generation",
        "category": "lineChart",
        "status": "Available",
        "primary_dashboard": "https://stage.sandbox.predera.com/id",
        "model_ref_id": "94da4f6c7c9649e5836c310766449bd1",
        "additional_report_ids": "\"1123\", \"3494\"",
        "created_at": "2022-07-05T10:20:22.515729",
        "created_by": "vijaykumar.guntreddy@predera.com",
        "updated_at": "2022-07-05T10:20:22.515729",
        "updated_by": "srikanth.thirumala@predera.com",
        "app_image_url": modelDevelopment,
        "app_id": "Code-Assist",
        "versions": [
            { 
                "value": "Code-Assist-7b", 
                "label": "Code-Assist-7b",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 1
                }
             },
            { 
                "value": "Code-Assist-13b", 
                "label": "Code-Assist-13b",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 2
                }
            },
            { 
                "value": "Code-Assist-70b", 
                "label": "Code-Assist-70b",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 8
                }
            },
        ]
    },
    {
        "id": 3,
        "name": "Pie Chart",
        "sourceType": "PIE_CHART",
        "finetuned": false,
        "description": "Llama 2 fine-tuned and optimized for dialogue use cases.",
        "Type": "text-generation",
        "category": "pieChart",
        "status": "Available",
        "primary_dashboard": "https://stage.sandbox.predera.com/id",
        "model_ref_id": "94da4f6c7c9649e5836c310766449bd1",
        "additional_report_ids": "\"1123\", \"3494\"",
        "created_at": "2022-07-05T10:20:22.515729",
        "created_by": "vijaykumar.guntreddy@predera.com",
        "updated_at": "2022-07-05T10:20:22.515729",
        "updated_by": "srikanth.thirumala@predera.com",
        "app_image_url": pricing,
        "app_id": "Llama-2-Chat",
        "versions": [
            { 
                "value": "Llama-2-7b-chat", 
                "label": "Llama-2-7b-chat",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 1
                }
             },
            { 
                "value": "Llama-2-13b-chat", 
                "label": "Llama-2-13b-chat",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 2
                }
            },
            { 
                "value": "Llama-2-70b-chat", 
                "label": "Llama-2-70b-chat",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 8
                }
            },
        ]
    },
]