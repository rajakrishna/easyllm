// components/Visualizations.tsx
'use client'
import * as React from 'react'
import {Typography, Card, Box, Grid, CardContent, CssBaseline} from '@mui/material'

import {
  Bar,
  BarChart,
  CartesianGrid,
  Legend,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts'

interface VisualizationsProps {
  // You can define props as needed for your dashboard
}

const Visualizations: React.FC<VisualizationsProps> = () => {
  const [total, setTotal] = React.useState(0)
  const [totalCost, setTotalCost] = React.useState(0)
  const [totalTokens, setTotalTokens] = React.useState(0)
  const [chat1Data, setChat1data] = React.useState<any[]>([])
  const [chat2Data, setChat2data] = React.useState<any[]>([])
  const [chat3Data, setChat3data] = React.useState<any[]>([])

  React.useEffect(() => {
    getStats()
  }, [])
  const getStats = () => {
    fetch('/easyllm/api/dashboard/', {
      cache: 'no-store',
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((data) => {
        let jsondata = data.buckets
        console.log('jsondata', jsondata)
        setTotal(JSON.parse(data.total_count))
        let arr1 = []
        let arr2 = []
        let arr3 = []
        let tokens = 0
        let cost = 0
        for (let i = 0; i <= jsondata.length; i++) {
          tokens = jsondata[i].tokens.value + tokens
          cost = jsondata[i].cost.value + cost
          let obj1 = {name: jsondata[i].key, requests: jsondata[i].doc_count, amt: 0}
          let obj2 = {
            name: jsondata[i].key,
            cost: parseFloat(jsondata[i].cost.value.toFixed(4)),
            amt: 0,
          }
          let obj3 = {name: jsondata[i].key, tokens: jsondata[i].tokens.value, amt: 0}
          arr1.push(obj1)
          arr2.push(obj2)
          arr3.push(obj3)
          setChat1data(arr1)
          setChat2data(arr2)
          setChat3data(arr3)
          setTotalCost(parseFloat(cost.toFixed(4)))
          setTotalTokens(tokens)
        }
      })
      .catch((err) => console.log('Error fetching access stats', err))
  }

  return (
    <Box>
      <Box
        gap={3}
        display='flex'
        flexWrap='wrap'
        justifyContent='start'
        overflow='auto'
        paddingY={2}
        paddingX={1}
      >
        <Grid container rowSpacing={1} columnSpacing={{xs: 1, sm: 2, md: 3}}>
          <Grid item xs={4}>
            <Card
              sx={{
                display: 'flex',
                width: 426,
                padding: 3,
              }}
            >
              <CardContent>
                <Typography sx={{fontSize: 14}} color='text.secondary' gutterBottom>
                  Total Cost
                </Typography>
                <Typography variant='h5' component='div'>
                  $ {totalCost}
                </Typography>
              </CardContent>
            </Card>
          </Grid>

          <Grid item xs={4}>
            <Card
              sx={{
                display: 'flex',
                width: 426,
                padding: 3,
              }}
            >
              <CardContent>
                <Typography sx={{fontSize: 14}} color='text.secondary' gutterBottom>
                  Total Tokens
                </Typography>
                <Typography variant='h5' component='div'>
                  {totalTokens}
                </Typography>
              </CardContent>
            </Card>
          </Grid>
          <Grid item xs={4}>
            <Card
              sx={{
                display: 'flex',
                width: 426,
                padding: 3,
              }}
            >
              <CardContent>
                <Typography sx={{fontSize: 14}} color='text.secondary' gutterBottom>
                  Total Requests
                </Typography>
                <Typography variant='h5' component='div'>
                  {total}
                </Typography>
              </CardContent>
            </Card>
          </Grid>

          <Grid item xs={4}>
           
              <Card
                sx={{
                  width: 426,
                  height: 300,
                  padding: 3,
                  marginTop: 10,
                }}
              >
                <Typography sx={{fontSize: 18, textAlign: 'center'}} variant='h5'>
                  Cost per Model{' '}
                </Typography>

                <CssBaseline />
                <ResponsiveContainer width='100%' height='100%'>
                  <BarChart data={chat2Data}>
                    <XAxis dataKey='name' tick={false} />
                    <YAxis />
                    <CartesianGrid strokeDasharray='3 3' />
                    <Tooltip />
                    <Legend />
                    <Bar
                      dataKey='cost'
                      fill='#F25417
'
                    />
                  </BarChart>
                </ResponsiveContainer>
              </Card>
          
          </Grid>
          <Grid item xs={4}>
              <Card
                sx={{
                  width: 426,
                  height: 300,
                  padding: 3,
                  marginTop: 10,
                }}
              >
                <Typography sx={{fontSize: 18, textAlign: 'center'}}>Tokens per Model </Typography>
                <CssBaseline />
                <ResponsiveContainer width='100%' height='100%'>
                  <BarChart data={chat3Data}>
                    <XAxis dataKey='name' tick={false} />
                    <YAxis />
                    <CartesianGrid strokeDasharray='3 3' />
                    <Tooltip />
                    <Legend />
                    <Bar
                      dataKey='tokens'
                      fill='#27AAE1
'
                    />
                  </BarChart>
                </ResponsiveContainer>
              </Card>
            
          </Grid>
          <Grid item xs={4}>
              <Card
                sx={{
                  width: 426,
                  height: 300,
                  padding: 3,
                  marginTop: 10,
                }}
              >
                <Typography sx={{fontSize: 18, textAlign: 'center'}}>Requests per Model </Typography>
                <CssBaseline />
                <ResponsiveContainer width='100%' height='100%'>
                  <BarChart data={chat1Data}>
                    <XAxis dataKey='name' tick={false} />
                    <YAxis />
                    <CartesianGrid strokeDasharray='3 3' />
                    <Tooltip />
                    <Legend />
                    <Bar dataKey='requests' fill='#2B3990' />
                  </BarChart>
                </ResponsiveContainer>
              </Card>
            
          </Grid>
        </Grid>
      </Box>
    </Box>
  )
}
export default Visualizations
