import React from 'react'
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from 'recharts'
import {CssBaseline, Container, Typography} from '@mui/material'

const data1 = [
  {name: 'Predera/llama-v2-70b-chat', uv: 4000, amt: 2400},
  {name: 'Predera/mistral-7b-instruct-4k', uv: 3000, amt: 2210},
  {name: 'openai/gpt-3.5-turbo', uv: 2000, amt: 2290},
  {name: 'gpt-3.5-turbo', uv: 2780, amt: 2000},
  {name: 'Predera/llama-v2-13b-code-instruct', uv: 1890, amt: 2181},
]

function BarChartExample(data: any) {
  console.log('chat data', data)
  return (
    <Container style={{paddingTop: '20px', paddingLeft: '0px', marginRight: '20px'}}>
      <CssBaseline />
      <ResponsiveContainer width='100%' height='100%'>
        <BarChart data={data1}>
          <XAxis dataKey='name' />
          <YAxis />
          <CartesianGrid strokeDasharray='3 3' />
          <Tooltip />
          <Legend />
          <Bar dataKey='uv' fill='#8884d8' />
          <Bar dataKey='pv' fill='#82ca9d' />
        </BarChart>
      </ResponsiveContainer>
    </Container>
  )
}

export default BarChartExample
