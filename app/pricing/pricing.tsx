"use client"

// components/Pricing.tsx
import React from 'react';
import { Card, CardContent, Typography } from '@mui/material';

interface PricingProps {
  title: string;
  description: string;
}

const Pricing: React.FC<PricingProps> = ({ title, description }) => {
  return (
    <Card>
      <CardContent>
        <Typography variant="h5" component="div">
          {title}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {description}
        </Typography>
      </CardContent>
    </Card>
  );
};

export default Pricing;
