import { Box } from "@mui/material"
import Pricing from "./pricing"

export default function Page() {
  return (
    <Box paddingLeft={5} paddingRight={5} paddingTop={2}>
      <Pricing title="Pricing" description="Pricing Description" />
    </Box>
  )
}
