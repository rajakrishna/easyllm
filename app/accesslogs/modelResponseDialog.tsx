import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog, { DialogProps } from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import {marked} from 'marked';
import {Box, Typography, List, ListItem, Tooltip} from '@mui/material';

interface ChildProps {
  modelResponse: string
  title: string
}

export default function ModelResponseDialog(props: ChildProps) {
  const [open, setOpen] = React.useState(false);
  const [scroll, setScroll] = React.useState<DialogProps['scroll']>('paper');

  const handleClickOpen = (scrollType: DialogProps['scroll']) => () => {
    setOpen(true);
    setScroll(scrollType);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const codeBlock = (code: string) => {
    const markedCode = marked.parse(code, {breaks: true})
    return <Box dangerouslySetInnerHTML={{__html: markedCode}} />
  }

  const renderMarkedResponse = (msg: any) => {
    const tokens = marked.lexer(msg)
    console.log('renderMarkedResponse :: msg :: ', msg)
    return (
      <div>
        {tokens.map((token: any) => {
          let tokenItem: any
          switch (token.type) {
            case 'paragraph':
              tokenItem = (
                <Typography variant='body2' paragraph={true} gutterBottom>
                  {token.text}
                </Typography>
              )
              break
            case 'code':
              tokenItem = <Box mb={2}>{codeBlock(token.raw)}</Box>
              break
            case 'space':
              tokenItem = <Typography variant='body2' paragraph={true} gutterBottom></Typography>
              break
            case 'list':
              tokenItem = (
                <List style={{listStyleType: 'none'}}>
                  {token.items.map((listItem: any) => (
                    <ListItem key={listItem.raw}>{listItem.raw}</ListItem>
                  ))}
                </List>
              )
              break

            default:
              break
          }
          return tokenItem
        })}
      </div>
    )
  }

  const descriptionElementRef = React.useRef<HTMLElement>(null);
  React.useEffect(() => {
    if (open) {
      const { current: descriptionElement } = descriptionElementRef;
      if (descriptionElement !== null) {
        descriptionElement.focus();
      }
    }
  }, [open]);


  return (
    <div>
      <Tooltip title="View">
        <Button 
        variant="text" 
        color='inherit' 
        onClick={handleClickOpen('paper')}
        sx={{textTransform: 'none',
            fontSize: 'inherit',
            padding: 0,
            '&:hover': {
                bgcolor: 'transparent',
            }}}
        >
        <Typography 
            maxWidth={300} 
            maxHeight={20} 
            overflow='hidden'
            align='left'
            fontSize='inherit'
        >
            {`${props.modelResponse}`}
        </Typography>
        </Button>
      </Tooltip>
      <Dialog
        open={open}
        onClose={handleClose}
        scroll={scroll}
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
        <DialogTitle id="scroll-dialog-title">{props.title}</DialogTitle>
        <DialogContent dividers={scroll === 'paper'}>
          <DialogContentText
            id="scroll-dialog-description"
            ref={descriptionElementRef}
            tabIndex={-1}
          >
            {renderMarkedResponse(props.modelResponse)}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Close</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}