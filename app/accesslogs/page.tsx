'use client'
import * as React from 'react'
import LogsTable from './logsTable'

export const dynamic = 'force-dynamic'

export default function Page() { return (<LogsTable />) }
