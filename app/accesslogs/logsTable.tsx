import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import { Avatar, Box, IconButton, TablePagination, Tooltip, Typography } from '@mui/material';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import ModelResponseDialog from './modelResponseDialog';
import { modelsData } from '../playground/playgroundData';
import { Refresh } from '@mui/icons-material';

export const dynamic = 'force-dynamic'

export default function BasicTable() {
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const [logs, setLogs] = React.useState<any[]>([])

    const getLogs = () => {
        fetch(`/easyllm/api/accesslogs/?page=${page}`, {
        cache: 'no-store',
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        }
        }).then((res) => res.json())
        .then((data) => setLogs(data))
        .catch(err => console.log("Error fetching access logs", err))
    }

    React.useEffect(() => {
        getLogs();
    }, [page])

    const handleChangePage = (event: unknown, newPage: number) => {
        setPage(newPage);
      };
    
      const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
      };


  return (
    <Box pl={4} pr={5}>
        <Box display="flex" flexDirection="row" alignItems="center">
            <Typography gutterBottom paddingLeft={1} marginBottom={0} variant='h6' component='div'>
                Accesss Logs
            </Typography>
            <Tooltip title="Refresh">
                <IconButton onClick={() => getLogs()}><Refresh /></IconButton>
            </Tooltip>
        </Box>

      <Box my={2} mr={2}>
        <Paper sx={{ width: '100%', overflow: 'hidden' }}>
            <TableContainer sx={{ maxHeight: '70vh', overflowY: 'auto'}}>
                <Table sx={{ minWidth: 650}} stickyHeader aria-label="sticky table">
                    <TableHead>
                        <TableRow>
                            <TableCell sx={{fontWeight: 'bold'}}>Time</TableCell>
                            <TableCell sx={{fontWeight: 'bold'}} align="left">Model</TableCell>
                            <TableCell sx={{fontWeight: 'bold'}} align="left">Prompt</TableCell>
                            <TableCell sx={{fontWeight: 'bold'}} align="left">Response</TableCell>
                            <TableCell sx={{fontWeight: 'bold'}} align="center">Tokens</TableCell>
                            <TableCell sx={{fontWeight: 'bold'}} align="center">Cost</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {logs.map((row) => (
                            <TableRow
                            key={`${row.start_time}`}
                            sx={{    '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                            <TableCell component="th" scope="row">
                                {new Date(row.start_time).toLocaleString()}
                            </TableCell>
                            <TableCell align="left">
                                <Box display="flex" flexDirection="row" alignItems="center">
                                    <Avatar src={modelsData.find((m) => row.model.includes(m.value))?.logo} />
                                    {row.model}
                                </Box>
                            </TableCell>
                            <TableCell align="left">
                                <ModelResponseDialog modelResponse={row.messages[0].content} title="Prompt" />
                            </TableCell>
                            <TableCell align="left">
                                <ModelResponseDialog modelResponse={row.completion} title="Response" />
                            </TableCell>
                            <TableCell align="right">{row.total_tokens}</TableCell>
                            <TableCell align="right">{`$ ${row.total_cost.toFixed(6)}`}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            <TablePagination
                rowsPerPageOptions={[10, 20]}
                component="div"
                count={100}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
        />
        </Paper>
      </Box>
    </Box>
  );
}
