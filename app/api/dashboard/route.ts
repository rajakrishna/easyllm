export const dynamic = 'force-dynamic'

export async function GET() {
  console.log('Dashboard Request')
  try {
    const res = await fetch(`${process.env.OPENAI_PROXY_HOST}/api/logs/stats?field=model`, {
      cache: 'no-store',
      method: 'GET',
      headers: {
        Authorization: `Bearer ${process.env.OPENAI_API_KEY}`,
      },
    })
    const result = await res.json()
    console.log('Dashboard Results', result, {flush: true})

    return Response.json(result)
  } catch (error) {
    console.log('Failed to get Dashboard Results', error, {flush: true})
    return Response.json([])
  }
}
