import OpenAI from 'openai'
import {OpenAIStream, StreamingTextResponse} from 'ai'

// Create an OpenAI API client (that's edge friendly!)
const openai = new OpenAI({
  apiKey: process.env.OPENAI_API_KEY,
  baseURL: process.env.OPENAI_API_HOST,
})

// IMPORTANT! Set the runtime to edge
export const runtime = 'edge'

export async function POST(req: Request) {
  // Extract the `messages` from the body of the request

  const reqData = await req.json()
  console.log('Chat Request: ', JSON.stringify(reqData))
  const {
    provider, 
    model, 
    messages, 
    max_tokens, 
    temperature, 
    top_p, 
    top_k, 
    frequency_penalty, 
    presence_penalty,
    stop
  } = reqData

  // Ask OpenAI for a streaming chat completion given the prompt
  const response = await openai.chat.completions.create({
    model: `${provider}/${model}`,
    stream: true,
    max_tokens: max_tokens,
    temperature: temperature,
    top_p: top_p,
    // top_k: top_k,
    frequency_penalty: frequency_penalty,
    presence_penalty: presence_penalty,
    stop: stop,
    messages,
  })
  // Convert the response into a friendly text-stream
  const stream = OpenAIStream(response)
  // Respond with the stream
  return new StreamingTextResponse(stream)
}
