import { NextRequest } from "next/server";
export const dynamic = 'force-dynamic'

export async function GET(req:NextRequest) {
  console.log('Logs Request')
  const page = req.nextUrl.searchParams.get('page');
  try {
    const res = await fetch(`${process.env.OPENAI_PROXY_HOST}/api/logs?page=${page}`, {
      cache: 'no-store',
      method: 'GET',
      headers: {
        Authorization: `Bearer ${process.env.OPENAI_API_KEY}`,
      },
    })
    const result = await res.json()
    return Response.json(result)
  } catch (error) {
    console.log('Failed to get LLM Access Logs', error, {flush: true})
    return Response.json([])
  }
}
