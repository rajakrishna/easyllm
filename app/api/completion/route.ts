import OpenAI from 'openai'
import {OpenAIStream, StreamingTextResponse} from 'ai'

// Create an OpenAI API client (that's edge friendly!)
const openai = new OpenAI({
  apiKey: process.env.OPENAI_API_KEY,
  baseURL: process.env.OPENAI_API_HOST,
})

// Set the runtime to edge for best performance
export const runtime = 'edge'

export async function POST(req: Request) {
  const {provider, model, prompt} = await req.json()
  console.log('Provider: ', provider)
  console.log('Model: ', model)
  console.log('Prompt: ', prompt)

  // Ask OpenAI for a streaming completion given the prompt
  const response = await openai.completions.create({
    model: `${provider}/${model}`,
    stream: true,
    temperature: 0.6,
    max_tokens: 300,
    prompt: prompt,
  })
  // Convert the response into a friendly text-stream
  const stream = OpenAIStream(response)
  // Respond with the stream
  return new StreamingTextResponse(stream)
}
