export async function POST(request: Request) {
  const data = await request.json()
  console.log('Cost Request: ', JSON.stringify(data))
  const res = await fetch(`${process.env.OPENAI_PROXY_HOST}/chat/completion-cost`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${process.env.OPENAI_API_KEY}`
    },
    body: JSON.stringify(data),
  })

  const result = await res.json()

  return Response.json(result)
}
