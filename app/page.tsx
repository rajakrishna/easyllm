import { Box, Typography } from "@mui/material"
import Dashboard from "./dashboard"
import Visualizations from "./visualizations/visualizations"

export default function Page() {
  return (
    <Box paddingLeft={4}>
      <Typography gutterBottom paddingLeft={1} marginBottom={0} variant="h6" component="div">
        Dashboard
      </Typography>
      {/* <Dashboard /> */}
      <Visualizations />
    </Box>
  )
}
