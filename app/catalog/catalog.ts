import openai_logo from './genai/openai_logo.png';
import Meta_AI_Llama from './genai/Meta_AI_Llama.webp';
import falcon_logo from './genai/tii_logo.svg';
import stability_ai_logo from './genai/stability_ai_logo.png';
import hf_logo from './genai/hf_logo.png';
import mistral from './genai/mistral.jpeg';
import Dolly_logo from './genai/Dolly_logo.png';

import { openaiDocumentation, openaiLicence, openaiOverview, openaiPrice, openaiUsecases } from './genai/models/openai';
import { huggingfaceDocumentation, huggingfaceLicence, huggingfaceOverview, huggingfacePrice, huggingfaceUsecases } from './genai/models/huggingface';
import { prederaDocumentation, prederaLicence, prederaOverview, prederaPrice, prederaUsecases } from './genai/models/predera';

export const GenAIModelCatalog = [
    {
        "id": 1,
        "name": "OpenAI",
        "sourceType": "LARGE_LANGUAGE_MODEL",
        "finetuned": false,
        "description": "Most capable GPT-3.5 model and optimized for chat.",
        "overview": openaiOverview,
        "usecases": openaiUsecases,
        "documentation": openaiDocumentation,
        "licence": openaiLicence,
        "price": openaiPrice,
        "Type": "text-generation",
        "category": "openai",
        "status": "Available",
        "primary_dashboard": "https://stage.sandbox.com/id",
        "model_ref_id": "94da4f6c7c9649e5836c310766449bd1",
        "additional_report_ids": "\"1125\", \"5294\"",
        "created_at": "2022-07-05T10:19:59.670676",
        "created_by": "srikanth.thirumala@predera.com",
        "updated_at": "2022-07-05T10:19:59.670676",
        "updated_by": "vijaykumar.guntreddy@predera.com",
        "app_image_url": openai_logo,
        "app_id": "openai",
        "versions": [
            {
                "value": "gpt-3.5-turbo-0301", 
                "label": "gpt-3.5-turbo-0301",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 0
                }
            },
            {
                "value": "gpt-3.5-turbo-0613", 
                "label": "gpt-3.5-turbo-0613","metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 0
                }
            }
        ]
    },
    {
        "id": 20,
        "name": "Huggingface",
        "sourceType": "HUGGING_FACE_HUB",
        "finetuned": false,
        "description": "Deploy any model from huggingface.",
        "overview": huggingfaceOverview,
        "usecases": huggingfaceUsecases,
        "documentation": huggingfaceDocumentation,
        "licence": huggingfaceLicence,
        "price": huggingfacePrice,
        "Type": "text-generation",
        "category": "huggingface",
        "status": "Available",
        "primary_dashboard": "https://stage.sandbox.predera.com/id",
        "model_ref_id": "94da4f6c7c9649e5836c310766449bd1",
        "additional_report_ids": "\"1123\", \"3494\"",
        "created_at": "2022-07-05T10:20:22.515729",
        "created_by": "vijaykumar.guntreddy@predera.com",
        "updated_at": "2022-07-05T10:20:22.515729",
        "updated_by": "srikanth.thirumala@predera.com",
        "app_image_url": hf_logo,
        "app_id": "Code-Assist",
        "versions": [
            { 
                "value": "Code-Assist-7b", 
                "label": "Code-Assist-7b",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 1
                }
             },
            { 
                "value": "Code-Assist-13b", 
                "label": "Code-Assist-13b",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 2
                }
            },
            { 
                "value": "Code-Assist-70b", 
                "label": "Code-Assist-70b",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 8
                }
            },
        ]
    },
    {
        "id": 9,
        "name": "Llama 2",
        "sourceType": "LARGE_LANGUAGE_MODEL",
        "finetuned": false,
        "description": "The next generation of Meta's open source large language model.",
        "overview": prederaOverview,
        "usecases": prederaUsecases,
        "documentation": prederaDocumentation,
        "licence": prederaLicence,
        "price": prederaPrice,
        "Type": "text-generation",
        "category": "llama-2",
        "status": "Available",
        "primary_dashboard": "https://stage.predera.com/",
        "model_ref_id": "f662401899a94b5f98253579b163a99d",
        "additional_report_ids": "\"2239\", \"1193\"",
        "created_at": "2022-07-05T10:23:51.229208",
        "created_by": "srikanth.thirumala@predera.com",
        "updated_at": "2022-07-05T10:23:51.229208",
        "updated_by": "srikanth.thirumala@predera.com",
        "app_image_url": Meta_AI_Llama,
        "app_id": "Llama 2",
        "versions": [
            { 
                "value": "Llama-2-7b", 
                "label": "Llama-2-7b",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 1
                }
            },
            { 
                "value": "Llama-2-13b", 
                "label": "Llama-2-13b",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 2
                }
             },
            { 
                "value": "Llama-2-70b", 
                "label": "Llama-2-70b",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 8
                }
            }
        ]
    },
    {
        "id": 10,
        "name": "Llama 2 Chat",
        "sourceType": "LARGE_LANGUAGE_MODEL",
        "finetuned": false,
        "description": "Llama 2 fine-tuned and optimized for dialogue use cases.",
        "overview": prederaOverview,
        "usecases": prederaUsecases,
        "documentation": prederaDocumentation,
        "licence": prederaLicence,
        "price": prederaPrice,
        "Type": "text-generation",
        "category": "llama-2-chat",
        "status": "Available",
        "primary_dashboard": "https://stage.sandbox.predera.com/id",
        "model_ref_id": "94da4f6c7c9649e5836c310766449bd1",
        "additional_report_ids": "\"1123\", \"3494\"",
        "created_at": "2022-07-05T10:20:22.515729",
        "created_by": "vijaykumar.guntreddy@predera.com",
        "updated_at": "2022-07-05T10:20:22.515729",
        "updated_by": "srikanth.thirumala@predera.com",
        "app_image_url": Meta_AI_Llama,
        "app_id": "Llama-2-Chat",
        "versions": [
            { 
                "value": "Llama-2-7b-chat", 
                "label": "Llama-2-7b-chat",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 1
                }
             },
            { 
                "value": "Llama-2-13b-chat", 
                "label": "Llama-2-13b-chat",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 2
                }
            },
            { 
                "value": "Llama-2-70b-chat", 
                "label": "Llama-2-70b-chat",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 8
                }
            },
        ]
    },
    {
        "id": 12,
        "name": "Code Llama",
        "sourceType": "LARGE_LANGUAGE_MODEL",
        "finetuned": false,
        "description": "Code Llama is a code generation model built on Llama 2.",
        "overview": prederaOverview,
        "usecases": prederaUsecases,
        "documentation": prederaDocumentation,
        "licence": prederaLicence,
        "price": prederaPrice,
        "Type": "text-generation",
        "category": "code-llama",
        "status": "Available",
        "primary_dashboard": "https://stage.sandbox.predera.com/id",
        "model_ref_id": "94da4f6c7c9649e5836c310766449bd1",
        "additional_report_ids": "\"1123\", \"3984\"",
        "created_at": "2022-07-05T10:19:59.670676",
        "created_by": "vijaykumar.guntreddy@predera.com",
        "updated_at": "2022-07-05T10:19:59.670676",
        "updated_by": "srikanth.thirumala@predera.com",
        "app_image_url": Meta_AI_Llama,
        "app_id": "code-llama",
        "versions": [
            {
                "value": "Code Llama 7b", 
                "label": "Code Llama 7b",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 1
                }
            },
            {
                "value": "Code Llama 13b", 
                "label": "Code Llama 13b",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 2
                }
            },
            {
                "value": "Code Llama 34b", 
                "label": "Code Llama 34b",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 4
                }
            },
            {
                "value": "Code Llama-Instruct 7b", 
                "label": "Code Llama-Instruct 7b",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 1
                }
            },
            {
                "value": "Code Llama-Instruct 13b", 
                "label": "Code Llama-Instruct 13b",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 2
                }
            },
            {
                "value": "Code Llama-Instruct 34b", 
                "label": "Code Llama-Instruct 34b",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 4
                }
            },
            {
                "value": "Code Llama-Python 7b", 
                "label": "Code Llama-Python 7b",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 1
                }
            },
            {
                "value": "Code Llama-Python 13b", 
                "label": "Code Llama-Python 13b",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 2
                }
            },
            {
                "value": "Code Llama-Python 34b", 
                "label": "Code Llama-Python 34b",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 4
                }
            }
        ]
    },
    {
        "id": 17,
        "name": "tiiuae",
        "sourceType": "LARGE_LANGUAGE_MODEL",
        "finetuned": false,
        "description": "Falcon is an autoregressive decoder-only model built by TII.",
        "overview": prederaOverview,
        "usecases": prederaUsecases,
        "documentation": prederaDocumentation,
        "licence": prederaLicence,
        "price": prederaPrice,
        "Type": "text-generation",
        "category": "tiiuae",
        "status": "Available",
        "primary_dashboard": "https://stage.sandbox.predera.com/id",
        "model_ref_id": "a736a2a68ae74018bca74482e081873c",
        "additional_report_ids": "\"2239\", \"3949\"",
        "created_at": "2022-07-05T10:29:07.771793",
        "created_by": "srikanth.thirumala@predera.com",
        "updated_at": "2022-07-05T10:29:07.771793",
        "updated_by": "srikanth.thirumala@predera.com",
        "app_image_url": falcon_logo,
        "app_id": "falcon",
        "versions": [
            {
                "value": "falcon-7b", 
                "label": "falcon-7b",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 0
                }
            },
            {
                "value": "falcon-7b-instruct", 
                "label": "falcon-7b-instruct",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 0
                }
            },
            {
                "value": "falcon-40b", 
                "label": "falcon-40b",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 0
                }
            },
            {
                "value": "falcon-40b-instruct", 
                "label": "falcon-40b-instruct",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 0
                }
            }
        ]
    },
    {
        "id": 23,
        "name": "Dolly v2",
        "sourceType": "LARGE_LANGUAGE_MODEL",
        "finetuned": false,
        "description": "Instruction-following LLM trained on the Databricks machine learning platform.",
        "overview": prederaOverview,
        "usecases": prederaUsecases,
        "documentation": prederaDocumentation,
        "licence": prederaLicence,
        "price": prederaPrice,
        "Type": "text-generation",
        "category": "dolly-v2",
        "status": "Available",
        "primary_dashboard": "https://stage.sandbox.predera.com/id",
        "model_ref_id": "a736a2a68ae74018bca74482e081873c",
        "additional_report_ids": "\"2239\", \"3949\"",
        "created_at": "2022-07-05T10:29:07.771793",
        "created_by": "srikanth.thirumala@predera.com",
        "updated_at": "2022-07-05T10:29:07.771793",
        "updated_by": "srikanth.thirumala@predera.com",
        "app_image_url": Dolly_logo,
        "app_id": "dolly",
        "versions": [
            {
                "value": "dolly-v2-3b", 
                "label": "dolly-v2-3b",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 0
                }
            },
            {
                "value": "dolly-v2-7b", 
                "label": "dolly-v2-7b",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 0
                }
            },
            {
                "value": "dolly-v2-12b", 
                "label": "dolly-v2-12b",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 0
                }
            }
        ]
    },
    {
        "id": 20,
        "name": "Mistral Ai",
        "sourceType": "LARGE_LANGUAGE_MODEL",
        "finetuned": false,
        "description": "Frontier AI in your hands",
        "overview": prederaOverview,
        "usecases": prederaUsecases,
        "documentation": prederaDocumentation,
        "licence": prederaLicence,
        "price": prederaPrice,
        "Type": "text-generation",
        "category": "mistralai",
        "status": "Available",
        "primary_dashboard": "https://stage.sandbox.predera.com/id",
        "model_ref_id": "94da4f6c7c9649e5836c310766449bd1",
        "additional_report_ids": "\"1123\", \"3494\"",
        "created_at": "2022-07-05T10:20:22.515729",
        "created_by": "vijaykumar.guntreddy@predera.com",
        "updated_at": "2022-07-05T10:20:22.515729",
        "updated_by": "srikanth.thirumala@predera.com",
        "app_image_url": mistral,
        "app_id": "mistralai",
        "versions": [
            { 
                "value": "Mistral-7B-Instruct-v0.1", 
                "label": "Mistral-7B-Instruct-v0.1",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 1
                }
             },
            { 
                "value": "Mistral-7B-v0.1", 
                "label": "Mistral-7B-v0.1",
                "metrics": {
                    "cpu": 0.5,
                    "memory": 0.5,
                    "gpu": 2
                }
            }
        ]
    },
    // {
    //     "id": 13,
    //     "name": "dolly-v2-3b",
    //     "sourceType": "LARGE_LANGUAGE_MODEL",
    //     "finetuned": false,
    //     "description": "Optimize nurse staffing. This can help in optimizing cost and effective service delivery.",
    //     "Type": "Nurse Staffing",
    //     "category": "dolly",
    //     "status": "Available",
    //     "primary_dashboard": "https://stage.sandbox.predera.com/id",
    //     "model_ref_id": "94da4f6c7c9649e5836c310766449bd1",
    //     "additional_report_ids": "\"1223\", \"0909\"",
    //     "created_at": "2022-07-05T10:20:22.515729",
    //     "created_by": "vijaykumar.guntreddy@predera.com",
    //     "updated_at": "2022-07-05T10:20:22.515729",
    //     "updated_by": "srikanth.thirumala@predera.com",
    //     "app_image_url": Dolly_logo,
    //     "app_id": "nurse_staffing"
    // },
    // {
    //     "id": 14,
    //     "name": "dolly-v2-12b",
    //     "sourceType": "LARGE_LANGUAGE_MODEL",
    //     "finetuned": false,
    //     "description": "Predict the development of severe asthma exacerbations.",
    //     "Type": "Asthma Prediction",
    //     "category": "dolly",
    //     "status": "Available",
    //     "primary_dashboard": "https://stage.sandbox.predera.com/id",
    //     "model_ref_id": "94da4f6c7c9649e5836c310766449bd1",
    //     "additional_report_ids": "\"1223\", \"9394\"",
    //     "created_at": "2022-07-05T10:19:59.670676",
    //     "created_by": "vijaykumar.guntreddy@predera.com",
    //     "updated_at": "2022-07-05T10:19:59.670676",
    //     "updated_by": "vijaykumar.guntreddy@predera.com",
    //     "app_image_url": Dolly_logo,
    //     "app_id": "asthma_prediction"
    // },
    // {
    //     "id": 15,
    //     "name": "dolly-v2-7b",
    //     "sourceType": "LARGE_LANGUAGE_MODEL",
    //     "finetuned": false,
    //     "description": "Predict the duration of hospitalization. A patient can stay at hospital as long as he is ill.",
    //     "Type": "Length of Stay",
    //     "category": "dolly",
    //     "status": "Available",
    //     "primary_dashboard": "https://stage.sandbox.predera.com/id",
    //     "model_ref_id": "f662401899a94b5f98253579b163a99d",
    //     "additional_report_ids": "\"3399\", \"2933\"",
    //     "created_at": "2022-07-05T10:23:51.229208",
    //     "created_by": "srikanth.thirumala@predera.com",
    //     "updated_at": "2022-07-05T10:23:51.229208",
    //     "updated_by": "srikanth.thirumala@predera.com",
    //     "app_image_url": Dolly_logo,
    //     "app_id": "length_of_stay"
    // },
    // {
    //     "id": 16,
    //     "name": "dolly-v1-6b",
    //     "sourceType": "LARGE_LANGUAGE_MODEL",
    //     "finetuned": false,
    //     "description": "Reduce mortality by early prediction and diagnosis of Sepsis.",
    //     "Type": "Sepsis Prediction",
    //     "category": "dolly",
    //     "status": "Available",
    //     "primary_dashboard": "https://stage.sandbox.predera.com/id",
    //     "model_ref_id": "f662401899a94b5f98253579b163a99d",
    //     "additional_report_ids": "\"1292\", \"9939\"",
    //     "created_at": "2022-07-05T10:24:17.528183",
    //     "created_by": "vijaykumar.guntreddy@predera.com",
    //     "updated_at": "2022-07-05T10:24:17.528183",
    //     "updated_by": "srikanth.thirumala@predera.com",
    //     "app_image_url": Dolly_logo ,
    //     "app_id": "sepsis_prediction"
    // },
    // {
    //     "id": 11,
    //     "name": "Llama-Custom",
    //     "sourceType": "LARGE_LANGUAGE_MODEL",
    //     "finetuned": true,
    //     "description": "Llama 2 fine-tuned and optimized for dialogue use cases.",
    //     "Type": "text-generation",
    //     "category": "llama",
    //     "status": "Available",
    //     "primary_dashboard": "https://stage.sandbox.predera.com/id",
    //     "model_ref_id": "94da4f6c7c9649e5836c310766449bd1",
    //     "additional_report_ids": "\"1123\", \"3494\"",
    //     "created_at": "2022-07-05T10:20:22.515729",
    //     "created_by": "vijaykumar.guntreddy@predera.com",
    //     "updated_at": "2022-07-05T10:20:22.515729",
    //     "updated_by": "srikanth.thirumala@predera.com",
    //     "app_image_url": Meta_AI_Llama,
    //     "app_id": "Llama-2-Chat",
    //     "versions": [
    //         { 
    //             "value": "Llama-2-7b-chat", 
    //             "label": "Llama-2-7b-chat",
    //             "metrics": {
    //                 "cpu": 0.5,
    //                 "memory": 0.5,
    //                 "gpu": 1
    //             }
    //          },
    //         { 
    //             "value": "Llama-2-13b-chat", 
    //             "label": "Llama-2-13b-chat",
    //             "metrics": {
    //                 "cpu": 0.5,
    //                 "memory": 0.5,
    //                 "gpu": 2
    //             }
    //         },
    //         { 
    //             "value": "Llama-2-70b-chat", 
    //             "label": "Llama-2-70b-chat",
    //             "metrics": {
    //                 "cpu": 0.5,
    //                 "memory": 0.5,
    //                 "gpu": 8
    //             }
    //         },
    //     ]
    // },
    // {
    //     "id": 18,
    //     "name": "Stable Diffusion",
    //     "sourceType": "LARGE_LANGUAGE_MODEL",
    //     "finetuned": false,
    //     "description": "Generate and modify images based on text prompts.",
    //     "Type": "text-to-image",
    //     "category": "stabilityai",
    //     "status": "Available",
    //     "primary_dashboard": "https://stage.sandbox.predera.com/id",
    //     "model_ref_id": "94da4f6c7c9649e5836c310766449bd1",
    //     "additional_report_ids": "\"1221\", \"3829\"",
    //     "created_at": "2022-07-05T10:20:22.515729",
    //     "created_by": "vijaykumar.guntreddy@predera.com",
    //     "updated_at": "2022-07-05T10:20:22.515729",
    //     "updated_by": "srikanth.thirumala@predera.com",
    //     "app_image_url": stability_ai_logo,
    //     "app_id": "stable-diffusion",
    //     "versions": [
    //         {
    //             "value": "stable-diffusion-v1-1", 
    //             "label": "stable-diffusion-v1-1",
    //             "metrics": {
    //                 "cpu": 0.5,
    //                 "memory": 0.5,
    //                 "gpu": 0
    //             }
    //         },
    //         {
    //             "value": "stable-diffusion-v1-2", 
    //             "label": "stable-diffusion-v1-2",
    //             "metrics": {
    //                 "cpu": 0.5,
    //                 "memory": 0.5,
    //                 "gpu": 0
    //             }},
    //         {
    //             "value": "stable-diffusion-v1-3", 
    //             "label": "stable-diffusion-v1-3",
    //             "metrics": {
    //                 "cpu": 0.5,
    //                 "memory": 0.5,
    //                 "gpu": 0
    //             }},
    //         {
    //             "value": "stable-diffusion-v1-4", 
    //             "label": "stable-diffusion-v1-4",
    //             "metrics": {
    //                 "cpu": 0.5,
    //                 "memory": 0.5,
    //                 "gpu": 0
    //             }}
    //     ]
    // }
]