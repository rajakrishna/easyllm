// components/Overview.tsx
import React from 'react';
import { Card, CardContent, Divider, Typography } from '@mui/material';
import MarkdownRenderer from './markdownRenderer';

interface OverviewProps {
  title: string;
  description: string;
  overview: string;
}

const Overview: React.FC<OverviewProps> = ({ title, description, overview }) => {

  return (
    <Card>
      <CardContent>
        <Typography variant="h5" component="div">
          {title}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {description}
        </Typography>
        <Divider sx={{mb: 2, mt: 1}} />
        {/* <span>State :</span> */}
        {/* <MarkdownRenderer markdownContent={overview} />   */}
        <Typography>State: {overview}</Typography>
        <Typography>Model Url: {title}</Typography>
      </CardContent>
    </Card>
  );
};

export default Overview;
