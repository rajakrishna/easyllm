// components/Overview.tsx
import React from 'react';
import { Card, CardContent, Divider, Typography } from '@mui/material';
import MarkdownRenderer from './markdownRenderer';

interface LicencingProps {
  title: string;
  description: string;
  licence: string;
}

const Licencing: React.FC<LicencingProps> = ({ title, description, licence }) => {
  return (
    <Card>
      <CardContent>
        <Typography variant="h5" component="div">
          {title}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {description}
        </Typography>
        <Divider sx={{mt: 1, mb: 2}} />
        <MarkdownRenderer markdownContent={licence} />
      </CardContent>
    </Card>
  );
};

export default Licencing;
