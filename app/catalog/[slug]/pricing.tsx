// components/Overview.tsx
import React from 'react';
import { Card, CardContent, Divider, Typography } from '@mui/material';
import MarkdownRenderer from './markdownRenderer';

interface PricingProps {
  title: string;
  description: string;
  price: string
}

const Pricing: React.FC<PricingProps> = ({ title, description, price }) => {
  return (
    <Card>
      <CardContent>
        <Typography variant="h5" component="div">
          {title}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {description}
        </Typography>
        <Divider sx={{mt: 1, mb: 2}} />
        <MarkdownRenderer markdownContent={price} />
      </CardContent>
    </Card>
  );
};

export default Pricing;
