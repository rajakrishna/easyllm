// components/Overview.tsx
import React from 'react';
import { Card, CardContent, Typography, Divider } from '@mui/material';
import MarkdownRenderer from './markdownRenderer';

interface UsecasesProps {
  title: string;
  description: string;
  usecases: string;
}

const Usecases: React.FC<UsecasesProps> = ({ title, description, usecases }) => {
  return (
    <Card>
      <CardContent>
        <Typography variant="h5" component="div">
          {title}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {description}
        </Typography>
        <Divider sx={{marginBottom: 2, marginTop: 1}} />
        <MarkdownRenderer markdownContent={usecases} />
      </CardContent>
    </Card>
  );
};

export default Usecases;
