import { Box, Typography } from "@mui/material"
import TabsComponent from "./tabs"
import EnableModal from "./enableModal"

export default function Page({ params }: { params: { slug: string } }) {
    return (
      <Box paddingLeft={4}>
        {/* <Typography gutterBottom paddingLeft={1} marginBottom={0} variant="h6" component="div">
          {`${params.slug[0].toUpperCase() + params.slug.substring(1)}`}
        </Typography> */}
        <Box display="flex" paddingLeft={1} flexDirection="row" paddingRight={10} justifyContent="space-between">
          <TabsComponent />
        {/* <EnableModal /> */}
        </Box>
      </Box>
    )
  }