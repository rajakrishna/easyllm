// components/Overview.tsx
import React from 'react';
import { Card, CardContent, Divider, Typography } from '@mui/material';
import MarkdownRenderer from './markdownRenderer';

interface DocumentationProps {
  title: string;
  description: string;
  documentation: string;
}

const Documentation: React.FC<DocumentationProps> = ({ title, description, documentation }) => {
  return (
    <Card>
      <CardContent>
        <Typography variant="h5" component="div">
          {title}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {description}
        </Typography>
        <Divider sx={{mt:1, mb:2}} />
        <MarkdownRenderer markdownContent={documentation} />
      </CardContent>
    </Card>
  );
};

export default Documentation;