// components/EnableForm.tsx
import React, { useState } from 'react';
import {
  Button,
  TextField,
  Grid
} from '@mui/material';

interface EnableFormProps {
  onSubmit: (formData: any) => void;
}

const EnableForm: React.FC<EnableFormProps> = ({ onSubmit }) => {
  const [formData, setFormData] = useState({
    modelType: '',
    inputText: '',
    maxTokens: 50,
  });

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setFormData({
      ...formData,
      [name]: name === 'maxTokens' ? parseInt(value) : value,
    });
  };

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    onSubmit(formData);
  };

  return (
    <form noValidate autoComplete="off" onSubmit={handleSubmit}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <TextField
            fullWidth
            label="Model Type"
            name="modelType"
            value={formData.modelType}
            onChange={handleChange}
            variant="outlined"
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            label="Input Text"
            name="inputText"
            value={formData.inputText}
            onChange={handleChange}
            variant="outlined"
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            label="Max Tokens"
            name="maxTokens"
            type="number"
            value={formData.maxTokens}
            onChange={handleChange}
            variant="outlined"
          />
        </Grid>
        <Grid item xs={12}>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            disableElevation={true}
            sx={{
              borderRadius:50,
              pl:3,
              pr:3,
            }}
          >
            Enable
          </Button>
          <Button
            type="submit"
            variant="contained"
            color="inherit"
            disableElevation={true}
            sx={{
              borderRadius:50,
              pl:3,
              pr:3,
              ml:1
            }}
          >
            Cancel
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default EnableForm;
