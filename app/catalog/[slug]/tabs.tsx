// components/TabsComponent.tsx
"use client"
import React, { useState } from 'react';
import { Tab, Tabs, Box, Button, Snackbar } from '@mui/material';
import Overview from './overview';
import Usecases from './useCases';
import Documentation from './documentation';
import Licencing from './licencing';
import Pricing from './pricing';
import { usePathname } from 'next/navigation';
import { GenAIModelCatalog } from '../catalog';
import {modelsData} from '../../playground/playgroundData';
import {
  Card,
  CardContent,
  Typography,
  CardHeader,
  Avatar,
} from '@mui/material';
import SyntaxHighlighter from 'react-syntax-highlighter';
import Grid from '@mui/material/Grid';
import Chip from '@mui/material/Chip';
import CopyToClipboard from 'react-copy-to-clipboard';
import ContentCopyIcon from '@mui/icons-material/ContentCopy';





declare module 'react-syntax-highlighter' {
  const SyntaxHighlighter: any; // You might need to provide the actual type here.

}




const curlRequest = `
  curl https://sandbox.predera.com/openai-proxy/v1/chat/completions ,
  -H "Content-Type: application/json" ,
  -H "Authorization: Bearer $PREDERA_API_KEY" ,
  -d '{
    "model": "<model-id>",
    "messages": [
      {
        "role": "system",
        "content": "You are a helpful assistant."
      },
      {
        "role": "user",
        "content": "Hello!"
      }
    ],
    "max_tokens": 100,
    "temperature": 1,
    "stream": true,
    "metadata": {
      "project": "my-project",
      "environment": "dev"
    }
  }'
  `
const curlResponse = `{
  "id": "chatcmpl-123",
  "object": "chat.completion",
  "created": 1677652288,
  "model": "<model-id>",
  "choices": [{
    "index": 0,
    "message": {
      "role": "assistant",
      "content": "Hello there, how may I assist you today?",
    },
    "finish_reason": "stop"
  }],
  "usage": {
    "prompt_tokens": 9,
    "completion_tokens": 12,
    "total_tokens": 21
  }
}`

const pythonRequest = `
  import os
  import openai

  os.getenv("OPENAI_API_KEY") = "<your-api-key>"
  os.getenv("OPENAI_API_BASE") = "https://sandbox.predera.com/openai-proxy/v1"
  
  completion = openai.ChatCompletion.create(
    model="<model-id>",
    messages=[
      {"role": "system", "content": "You are a helpful assistant."},
      {"role": "user", "content": "Hello!"}
    ],
    "max_tokens": 100,
    "temperature": 1,
    "stream": false,
    "metadata": {
        "project": "my-project",
        "environment": "dev"
    }
  )
  
  print(completion.choices[0].message)`

  const longChainRequest = `
  import os
  from langchain.chat_models import ChatOpenAI
  from langchain.schema import AIMessage, HumanMessage, SystemMessage
  
  # Modify OpenAI's API key and API base to use Predera's LLM.
  openai.api_key = "<your-api-key-here>"
  openai.api_base = "https://sandbox.predera.com/openai-proxy/v1"
  # OR
  os.environ['OPENAI_API_KEY'] = "<your-api-key-here>"
  os.environ['OPENAI_API_BASE'] = "https://sandbox.predera.com/openai-proxy/v1"
  
  chat = ChatOpenAI(
      model_name="<model-id>"
  )
  
  messages = [
    SystemMessage(content="You are a helpful assistant that translates English to French."),
    HumanMessage(content="I love programming.")
  ]
  chat.predict_messages(messages)
  
  for s in chat.stream(messages):
      print(s.content, end="", flush=True)
  
  # Response
  # J'adore la programmation.`



const TabsComponent: React.FC = () => {
  const [value, setValue] = useState(0);
  const [copied, setCopied] = useState(false);

  const handleCopy = () => {
      setCopied(true);
  };

  const handleCloseSnackbar = () => {
    setCopied(false);
};

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

   const pathname = usePathname();
   const pathnames = pathname.split('/').filter((x) => x);

   const modelCategory = pathnames[pathnames.length - 1];

  // const modelObj = GenAIModelCatalog.find(ele => ele.category === modelCategory);
 
  const modelObj = modelsData.find(ele => ele.value === modelCategory);

  return (
    <Box>
          <Card>
      <CardHeader
        title={modelObj?.value}
        subheader={modelObj?.provider}
        avatar={<Avatar src={modelObj?.logo} />}
      />
      <CardContent sx={{width:'70vw',marginRight:'1rem'}}>
      <Grid container spacing={2} marginBottom={2}>
          <Grid item xs={2}>
            <Typography variant="body1" component="div" fontWeight="bold">Description</Typography>
          </Grid>
          <Grid item xs={10}>
            <Typography>{modelObj?.description}</Typography>
          </Grid>
          <Grid item xs={2}>
            <Typography variant="body1" component="div" fontWeight="bold">Status</Typography>
          </Grid>
          <Grid item xs={10}>
            {/* <Typography>{modelObj?.state}</Typography> */}
            <Chip label={modelObj?.state.toUpperCase()} color="success" size="small"/>
          </Grid>
          <Grid item xs={2}>
            <Typography variant="body1" component="div" fontWeight="bold">Model Id</Typography>
          </Grid>
          <Grid item xs={10}>
            <Typography>{`${modelObj?.provider}/${modelObj?.value}`}</Typography>
          </Grid>
        </Grid>
        <Typography variant="h6" fontWeight="bold"  marginTop={4}>How to Use</Typography>
        <Grid container spacing={2} marginTop={2}>
          <Grid item xs={6}>
            <Typography fontWeight="bold">Curl Request</Typography>
            <Box position="relative">
            <Box position="absolute" top={8} right={8} zIndex={1}>
              <CopyToClipboard text={curlRequest}
                  onCopy={handleCopy}
                  >
                    <Button                             
                      variant="text"
                      color="primary"
                      size="small"
                      >
                        <ContentCopyIcon />
                    </Button>
                </CopyToClipboard>
              </Box>
              <SyntaxHighlighter language="javascript" >
                  {curlRequest}
            </SyntaxHighlighter>
            <Snackbar 
              open={copied}
              autoHideDuration={2000}
              onClose={handleCloseSnackbar}
              message="Copied to clipboard"
            />

        </Box>
          </Grid>

          <Grid item xs={6}>
            <Typography fontWeight="bold">Response</Typography>
            <Box position="relative">
            <Box position="absolute" top={8} right={8} zIndex={1}>
              <CopyToClipboard text={curlResponse}
                  onCopy={handleCopy}
                  >
                    <Button                             
                      variant="text"
                      color="primary"
                      size="small"
                      >
                        <ContentCopyIcon />
                    </Button>
                </CopyToClipboard>
              </Box>
            <SyntaxHighlighter language="javascript" >
                  {curlResponse}
            </SyntaxHighlighter>
            <Snackbar 
              open={copied}
              autoHideDuration={2000}
              onClose={handleCloseSnackbar}
              message="Copied to clipboard"
            />

        </Box>
          </Grid>
        </Grid>

       <Typography marginTop={4} fontWeight="bold">Python</Typography>
       <Box position="relative">
       <Box position="absolute" top={8} right={8} zIndex={1}>
       <CopyToClipboard text={pythonRequest}
          onCopy={handleCopy}
          >
            <Button                             
              variant="text"
              color="primary"
              size="small"
              >
                <ContentCopyIcon />
            </Button>
        </CopyToClipboard>
        </Box>
          <SyntaxHighlighter language="python" >
                  {pythonRequest}
            </SyntaxHighlighter>
            <Snackbar 
              open={copied}
              autoHideDuration={2000}
              onClose={handleCloseSnackbar}
              message="Copied to clipboard"
            />

        </Box>
      
       <Typography marginTop={4} fontWeight="bold">LangChain</Typography>
       <Box position="relative">
       <Box position="absolute" top={8} right={8} zIndex={1}>
       <CopyToClipboard text={longChainRequest}
          onCopy={handleCopy}
          >
            <Button                             
              variant="text"
              color="primary"
              size="small"
              >
                <ContentCopyIcon />
            </Button>
        </CopyToClipboard>
       </Box>
       <SyntaxHighlighter>
              {longChainRequest}
        </SyntaxHighlighter>
        <Snackbar 
              open={copied}
              autoHideDuration={2000}
              onClose={handleCloseSnackbar}
              message="Copied to clipboard"
            />

        </Box>
      </CardContent>
    </Card>
        {/* <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
        >
          <Tab  label="Overview" />
          <Tab label="Use Cases" />
          <Tab label="Documentation" />
          <Tab label="Licence" />
          <Tab label="Pricing" />
        </Tabs> */}

    </Box>
  );
};

export default TabsComponent;
