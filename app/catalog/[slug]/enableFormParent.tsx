// pages/index.tsx
import React from 'react';
import EnableForm from './enableForm';
import { Box } from '@mui/material';

export default function EnableFormParent() {
  const handleSubmit = (formData: any) => {
    // Handle model deployment logic here
    console.log('Form Data:', formData);
    // You can send this data to an API or perform model deployment actions.
  };

  return (
    <Box paddingTop={1}>
      <EnableForm onSubmit={handleSubmit} />
    </Box>
  );
}
