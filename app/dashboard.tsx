// components/Dashboard.tsx
"use client"
import React, { useState, useEffect } from 'react';
import {Typography, Card, CardContent, CardMedia, Badge, Box } from '@mui/material';
import { DashboardCatalog } from './dashgoard';
import Link from 'next/link';

interface DashboardProps {
  // You can define props as needed for your dashboard
}

const Dashboard: React.FC<DashboardProps> = () => {
  const [data, setData] = useState<any[]>([{"title": ""}]);

  useEffect(() => {
    // Fetch data from your large language model or other sources
    // Example:
    // const fetchData = async () => {
    //   const response = await fetch('YOUR_API_ENDPOINT');
    //   const data = await response.json();
    //   setData(data);
    // };
    // fetchData();
  }, []);

  return (
    <Box>
        <Box 
          gap={3} 
          display="flex" 
          flexWrap="wrap" 
          justifyContent="start" 
          overflow="auto"
          paddingY={2}
          paddingX={1}
        >
        {DashboardCatalog.map((item)=>{
          return(
            <Link key={item.id} href={`/${item.category}`}>
                <Card 
                sx={{ 
                    display: "flex",
                    width: 426,
                    height: 175,
                }} 
                >
                {/* <CardActionArea> */}
                  <CardMedia
                    component="img"
                    // height="140"
                    // width="160"
                    sx={{
                        width: 200,
                        backgroundSize: 'contain'}}
                    image={item.app_image_url.src}
                    alt={item.name}
                  />
                <Box 
                sx={{
                      display: "flex",
                      flexDirection: "column",
                      width: "265"
                }}>
                  <CardContent
                  sx={{
                    paddingTop: '1.5rem',
                    paddingLeft: '1.5rem',
                    paddingRight: '1.5rem',
                    paddingBottom: '0',
                  }}
                  >
                    <Typography gutterBottom marginBottom={0} variant="h6" component="div">
                      {item.name}
                    </Typography>
                    <Box
                      display="flex"
                      flexDirection="row" // horizontal layout
                      alignItems="center"
                      justifyContent="space-between"
                      padding={2}
                    >
                      <Badge badgeContent={"LLM"} color="primary" />
                    </Box>
                    <Typography alignContent="start" variant="body2" color="text.secondary">
                      {item.description}
                    </Typography>
                  </CardContent>
                </Box>
                </Card>
            </Link>
          )
        })}
        </Box>
      </Box>
  );
};

export default Dashboard;
