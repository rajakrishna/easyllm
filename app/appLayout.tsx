'use client'
import * as React from 'react';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import AppBar from '@mui/material/AppBar';
import CssBaseline from '@mui/material/CssBaseline';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import Link from 'next/link';
import Breadcrumb from './breadCrumbs';
import Image from 'next/image';
import { usePathname } from 'next/navigation';
import { Typography } from '@mui/material';
import Collapse from '@mui/material/Collapse';

const drawerWidth = 240;

export default function AppLayout({children}:{children:React.ReactNode}) {

  const pathName = usePathname();
  const pathnames = pathName.split('/').filter((x) => x);
  let lastpath = 'dashboard';
  if (pathnames.length) {
    lastpath = pathnames[0]
  }
  
  return (
      <Box display="flex">
        <CssBaseline />
        <AppBar style={{background: 'white', boxShadow: 'none'}} position="fixed">
          <Toolbar style={{minHeight: '63px'}}>
            <Box marginLeft={29}>
              <Breadcrumb />
            </Box>
          </Toolbar>
          <Divider sx={{marginTop:0}} />

        </AppBar>
        <Drawer
          variant="permanent"
          sx={{
            width: drawerWidth,
            flexShrink: 0,
            [`& .MuiDrawer-paper`]: { width: drawerWidth, boxSizing: 'border-box' },
          }}
        >
          
          <Box sx={{ overflow: 'auto' }}>
              <Link href={'/'}>
                <ListItem style={{paddingTop: '0px'}} disablePadding>
                  <ListItemButton style={{paddingTop: '16px'}}>
                    <Image
                      src="/easyllm/images/predera_logo.png"
                      alt="PrederLogo"
                      width={150} // Desired image width
                      height={100} // Desired image height
                    />
                  </ListItemButton>
                </ListItem>
              </Link>
              <Divider sx={{marginTop:0}} />
              <Link href={`/`}>
                <ListItem sx={{background: `${lastpath === 'dashboard' ? '#e5e7eb' : ''}`}} style={{paddingTop: '0px'}} disablePadding>
                  <ListItemButton style={{paddingTop: '16px'}}>
                    <ListItemText>
                      <Typography  sx={{fontWeight:`${lastpath === 'dashboard' ? 'bolder' : 'lighter'}`}}>
                        Dashboard
                      </Typography>
                    </ListItemText>
                  </ListItemButton>
                </ListItem>
              </Link>

              <List style={{paddingTop: '0px'}}>                
              <Link href={`/catalog`}>
                    <ListItem sx={{background: `${lastpath === 'catalog' ? '#e5e7eb' : ''}`}} disablePadding>
                      <ListItemButton style={{paddingTop: '12px'}}>
                        <ListItemText>
                          <Typography  sx={{fontWeight:`${lastpath === 'catalog' ? 'bolder' : 'lighter'}`}}>
                            Catalog
                          </Typography>
                        </ListItemText>
                      </ListItemButton>
                    </ListItem>
                </Link>

                <Link href={`/playground`}>
                  <ListItem sx={{background: `${lastpath === 'playground' ? '#e5e7eb' : ''}`}} disablePadding>
                    <ListItemButton style={{paddingTop: '12px'}}>
                      <ListItemText>
                        <Typography  sx={{fontWeight:`${lastpath === 'playground' ? 'bolder' : 'lighter'}`}}>
                          Playground
                        </Typography> 
                      </ListItemText>
                    </ListItemButton>
                  </ListItem>
                </Link>

                <Link href={`/accesslogs`}>
                  <ListItem sx={{background: `${lastpath === 'logs' ? '#e5e7eb' : ''}`}} disablePadding>
                    <ListItemButton style={{paddingTop: '12px'}}>
                      <ListItemText disableTypography={true}>
                        <Typography  sx={{fontWeight:`${lastpath === 'accesslogs' ? 'bolder' : 'lighter'}`}}>
                          Access Logs
                        </Typography> 
                      </ListItemText>
                    </ListItemButton>
                  </ListItem>
                </Link>

                <Link href={`/applications`}>
                  <ListItem sx={{background: `${lastpath === 'logs' ? '#e5e7eb' : ''}`}} disablePadding>
                    <ListItemButton style={{paddingTop: '12px'}}>
                      <ListItemText disableTypography={true}>
                        <Typography  sx={{fontWeight:`${lastpath === 'applications' ? 'bolder' : 'lighter'}`}}>
                          Applications
                        </Typography> 
                      </ListItemText>
                    </ListItemButton>
                  </ListItem>
                </Link>

                <ListItem sx={{background: `${lastpath === 'logs' ? '#e5e7eb' : ''}`}} disablePadding>
                  <ListItemButton disabled style={{paddingTop: '12px'}}>
                    <ListItemText disableTypography={true}>
                      <Typography>
                        Coming Soon
                      </Typography> 
                    </ListItemText>
                  </ListItemButton>
                </ListItem>

                <ListItem sx={{paddingTop: 0, background: `${lastpath === 'logs' ? '#e5e7eb' : ''}`}}>

                  <Collapse in={true} timeout="auto" unmountOnExit>
                    <List component="div" sx={{paddingTop: 0}}>

                      <ListItem sx={{background: `${lastpath === 'logs' ? '#e5e7eb' : ''}`}} disablePadding>
                        <ListItemButton disabled style={{paddingTop: '12px'}}>
                          <ListItemText disableTypography={true}>
                            <Typography>
                              Datasets
                            </Typography> 
                          </ListItemText>
                        </ListItemButton>
                      </ListItem>

                      <ListItem sx={{background: `${lastpath === 'logs' ? '#e5e7eb' : ''}`}} disablePadding>
                        <ListItemButton disabled style={{paddingTop: '12px'}}>
                          <ListItemText disableTypography={true}>
                            <Typography>
                              Prompt Library
                            </Typography> 
                          </ListItemText>
                        </ListItemButton>
                      </ListItem>

                      <ListItem sx={{background: `${lastpath === 'logs' ? '#e5e7eb' : ''}`}} disablePadding>
                        <ListItemButton disabled style={{paddingTop: '12px'}}>
                          <ListItemText disableTypography={true}>
                            <Typography>
                              Fine Tuning
                            </Typography> 
                          </ListItemText>
                        </ListItemButton>
                      </ListItem>

                      <ListItem sx={{background: `${lastpath === 'logs' ? '#e5e7eb' : ''}`}} disablePadding>
                        <ListItemButton disabled style={{paddingTop: '12px'}}>
                          <ListItemText disableTypography={true}>
                            <Typography>
                              Evaluations
                            </Typography> 
                          </ListItemText>
                        </ListItemButton>
                      </ListItem>

                    </List>
                  </Collapse>

                </ListItem>

            </List>
          </Box>
        </Drawer>

        <Box style={{paddingLeft: '0px', paddingRight: '0px'}} component="main" sx={{ flexGrow: 1, p: 3 }}>
          <Toolbar />
          <div>
              {children}
          </div>
        </Box>
      </Box>
  );
}