import predera_logo from './images/predera_logo.png';

interface Application {
    id: string
    name: string
    description: string
    logo: string
    value: string
    url: string
}

export const applicationsData: Application[] = [
    {
        id: 'aiq_chat',
        name: 'AIQ Chat',
        description: 'Chat with deployed Large Language Models.',
        logo: predera_logo.src,
        value: 'aiq_chat_logo',
        url: 'https://stage.predera.com/chat/',
    },
    {
        id: 'rag',
        name: 'RAG',
        description: 'Retrieval Augmented Generation improves quality of LLMs.',
        logo: predera_logo.src,
        value: 'rag_logo',
        url: 'https://rag.sandbox.predera.com/',

    },
    {
        id: 'gradio',
        name: 'Gradio',
        description: 'Upload json file with Predictions, Reference keys and Run Evaluation',
        logo: predera_logo.src,
        value: 'gradio_logo',
        url: 'https://sandbox.predera.com/llm-evaluator/',
    },{
        id: 'code_translator',
        name: 'Code Translator',
        description: 'Enter code from 40+ languages and click "Translate" to translate code',
        logo: predera_logo.src,
        value: 'code_translator_logo',
        url: 'https://sandbox.predera.com/ai-code-translator/',
    },
]