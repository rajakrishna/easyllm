'use client'
import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { Badge, CardActionArea } from '@mui/material';
import { Box } from '@mui/system';
import { applicationsData } from './applicationsData';

export default function Catalog() {

    return (
      <Box paddingLeft={4}>
        <Typography gutterBottom paddingLeft={1} marginBottom={0} variant="h6" component="div">
          Applications
        </Typography>
        <Box 
          gap={3} 
          display="flex" 
          flexWrap="wrap" 
          justifyContent="start" 
          overflow="auto"
          paddingY={2}
          paddingX={1}
        >
        {applicationsData.map((item)=>{
          return(
            <a key={item.id} href={`${item.url}`} target='_blank'>
                <Card  key={item.id}>
                <CardActionArea sx={{ 
                    display: "flex",
                    width: 426,
                    height: 175,
                }} >
                    <CardMedia
                        component="img"
                        sx={{
                            width: 75,
                            backgroundSize: 'contain',
                            margin: 5
                        }}
                        image={item.logo}
                        alt={item.value}
                    />
                <Box 
                sx={{
                      display: "flex",
                      flexDirection: "column",
                      width: "265"
                }}>
                  <CardContent
                  sx={{
                    paddingTop: '1.5rem',
                    paddingLeft: '1.5rem',
                    paddingRight: '1.5rem',
                    paddingBottom: '0',
                  }}
                  >
                    <Typography gutterBottom marginBottom={0} variant="h6" component="div" sx={{height:'2rem',overflow:'hidden'}}>
                      {item.name}
                    </Typography>
                    <Box
                      display="flex"
                      flexDirection="row" // horizontal layout
                      alignItems="center"
                      justifyContent="space-between"
                      padding={2}
                    >
                      <Badge badgeContent={"App"} color="primary" />
                    </Box>
                    <Typography alignContent="start" variant="body2" color="text.secondary" sx={{height:'5.25rem',overflow:'hidden'}}>
                      {item.description}
                    </Typography>
                  </CardContent>
                </Box>
                </CardActionArea>
                </Card>
            </a>
          )
        })}
        </Box>
      </Box>
    )
  }